#!/usr/bin/env bash
sudo pacman -Syyy
sudo pacman -S --noconfirm --needed git git-lfs ansible-core
ansible-galaxy collection install community.general ansible.posix community.postgresql
git clone https://gitlab.com/MagicBytes/magicarch
cd magicarch && git lfs install && git-lfs pull
clear
ansible-playbook site.yml --connection=local --ask-become-pass