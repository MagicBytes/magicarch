class FilterModule:
    def filters(self):
        return {
            'numeric_compare': self.numeric_compare,
        }

    def numeric_compare(self, value, operator, operand):
        if operator == '>=':
            return int(value) >= int(operand)
        elif operator == '>':
            return int(value) > int(operand)
        elif operator == '<=':
            return int(value) <= int(operand)
        elif operator == '<':
            return int(value) < int(operand)
        elif operator == '==':
            return int(value) == int(operand)
        else:
            raise ValueError(f"Unsupported operator: {operator}")
