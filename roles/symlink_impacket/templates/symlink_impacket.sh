#!/bin/bash

# Directory where the original scripts are located
SCRIPT_DIR="$HOME/.local/bin"

# Prefix for the new symlink
PREFIX="impacket-"

# List of Impacket scripts
declare -a impacket_scripts=(
    "addcomputer"
    "atexec"
    "dcomexec"
    "dpapi"
    "dacledit"
    "esentutl"
    "exchanger"
    "findDelegation"
    "Get-GPPPassword"
    "GetADUsers"
    "getArch"
    "GetNPUsers"
    "getPac"
    "getST"
    "getTGT"
    "GetUserSPNs"
    "goldenPac"
    "karmaSMB"
    "kintercept"
    "lookupsid"
    "mimikatz"
    "mqtt_check"
    "mssqlclient"
    "mssqlinstance"
    "netview"
    "nmapAnswerMachine"
    "ntfs-read"
    "ntlmrelayx"
    "ping"
    "ping6"
    "psexec"
    "raiseChild"
    "rbcd"
    "rdp_check"
    "reg"
    "registry-read"
    "rpcdump"
    "rpcmap"
    "sambaPipe"
    "samrdump"
    "secretsdump"
    "services"
    "smbclient"
    "smbexec"
    "smbpasswd"
    "smbrelayx"
    "smbserver"
    "sniff"
    "sniffer"
    "split"
    "ticketConverter"
    "ticketer"
    "wmiexec"
    "wmipersist"
    "wmiquery"
)

# Loop through the list of Impacket scripts
for script_name in "${impacket_scripts[@]}"; do
    # Check if the script exists
    if [[ -f "${SCRIPT_DIR}/${script_name}.py" ]]; then
        # Create a symlink with the new name
        ln -s "${SCRIPT_DIR}/${script_name}.py" "${SCRIPT_DIR}/${PREFIX}${script_name}"
        chmod +x "${SCRIPT_DIR}/${PREFIX}${script_name}"
    else
        echo "Script ${script_name}.py not found in ${SCRIPT_DIR}"
    fi
done
