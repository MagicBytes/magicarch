---
#
# Setup ZSH
#

## Setup Oh-My-Zsh
- name: Install Oh-My-Zsh for user
  ansible.builtin.git:
    repo: https://github.com/ohmyzsh/ohmyzsh
    dest: "{{ lookup('env', 'HOME') }}/.oh-my-zsh"
  become: no

- name: Set permissions of Oh-My-Zsh for user
  ansible.builtin.file:
    path: "{{ lookup('env', 'HOME') }}/.oh-my-zsh"
    # Prevent the cloned repository from having insecure permissions. Failing to do
    # so causes compinit() calls to fail with "command not found: compdef" errors
    # for users with insecure umasks (e.g., "002", allowing group writability).
    mode: 'go-w'
    recurse: true
  become: no

- name: Install my personal zsh config for user
  ansible.builtin.copy:
    src: ../templates/.zshrc.j2
    dest: "{{ lookup('env', 'HOME') }}/.zshrc"
    owner: "{{ ansible_env.USER }}"
    group: "{{ ansible_env.USER }}"
    mode: "0644"
    remote_src: no
  become: no

- name: Install powerlevel10k for user
  ansible.builtin.git:
    repo: https://github.com/romkatv/powerlevel10k.git
    dest: "{{ lookup('env', 'HOME') }}/.oh-my-zsh/custom/themes/powerlevel10k"
  become: no

- name: Install powerlevel10k zsh config for user
  ansible.builtin.copy:
    src: ../templates/.p10k.zsh.j2
    dest: "{{ lookup('env', 'HOME') }}/.p10k.zsh"
    owner: "{{ ansible_env.USER }}"
    group: "{{ ansible_env.USER }}"
    mode: "0644"
    remote_src: no
  become: no

- name: Install zsh-autosuggestions plugin for user
  ansible.builtin.git:
    repo: https://github.com/zsh-users/zsh-autosuggestions.git
    dest: "{{ lookup('env', 'HOME') }}/.oh-my-zsh/custom/plugins/zsh-autosuggestions"
  become: no

- name: Install zsh-syntax-highlighting plugin for user
  ansible.builtin.git:
    repo: https://github.com/zsh-users/zsh-syntax-highlighting.git
    dest: "{{ lookup('env', 'HOME') }}/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting"
  become: no

- name: Install Oh-My-Zsh for root
  ansible.builtin.git:
    repo: https://github.com/ohmyzsh/ohmyzsh
    dest: "/root/.oh-my-zsh"
  become: yes

- name: Set permissions of Oh-My-Zsh for root
  ansible.builtin.file:
    path: "/root/.oh-my-zsh"
    # Prevent the cloned repository from having insecure permissions. Failing to do
    # so causes compinit() calls to fail with "command not found: compdef" errors
    # for users with insecure umasks (e.g., "002", allowing group writability).
    mode: 'go-w'
    recurse: true
  become: yes

- name: Install my personal zsh config for root
  ansible.builtin.copy:
    src: ../templates/.zshrc.j2
    dest: "/root/.zshrc"
    owner: "root"
    group: "root"
    mode: "0644"
    remote_src: no
  become: yes

- name: Install powerlevel10k for root
  ansible.builtin.git:
    repo: https://github.com/romkatv/powerlevel10k.git
    dest: "/root/.oh-my-zsh/custom/themes/powerlevel10k"
  become: yes

- name: Install powerlevel10k zsh config for root
  ansible.builtin.copy:
    src: ../templates/.p10k.zsh.j2
    dest: "/root/.p10k.zsh"
    owner: "root"
    group: "root"
    mode: "0644"
    remote_src: no
  become: yes

- name: Install zsh-autosuggestions plugin for root
  ansible.builtin.git:
    repo: https://github.com/zsh-users/zsh-autosuggestions.git
    dest: "/root/.oh-my-zsh/custom/plugins/zsh-autosuggestions"
  become: yes

- name: Install zsh-syntax-highlighting plugin for user
  ansible.builtin.git:
    repo: https://github.com/zsh-users/zsh-syntax-highlighting.git
    dest: "/root/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting"
  become: yes

- name: Set zsh as default shell for user
  command: usermod --shell /usr/bin/zsh {{ ansible_env.USER }}
  become: yes

- name: Set zsh as default shell for root
  command: usermod --shell /usr/bin/zsh root
  become: yes