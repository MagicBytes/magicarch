# MagicArch: Automated Arch Linux Setup for Pentesting

## by MagicBytes

![](logos/MagicArch_Logo_200px.png)

MagicArch is a comprehensive post-installation script built with Ansible, designed to transform a basic Arch Linux installation into a fully equipped Pentesting Distro customized for pentesting and performance. With a focus on security assessments, customization, and performance optimization, MagicArch simplifies the setup process and ensures a tailored environment for pentesting tasks.

<br>

## Features

- **Ansible-Powered Automation**: Leveraging the power of Ansible, MagicArch streamlines the setup process, automating the configuration of essential tools and settings.

- **Pentesting Tool Integration**: MagicArch includes a wide array of pentesting tools, conveniently configured and ready for use, enhancing efficiency and productivity in security assessments and penetration testing tasks.

- **Customization**: MagicArch deploys various customizations such as themes, wallpapers, desktop configurations, and more, ensuring a unique user experience.

- **Performance Optimization**: MagicArch incorporates performance tweaks and optimizations, enhancing system responsiveness and ensuring smooth operation during intensive tasks.

<br>

## Automated Quick Install (needs sudo & curl) (do not run as root):

```bash
curl -s -O https://gitlab.com/MagicBytes/magicarch/-/raw/main/install.sh && bash install.sh
```

<br>

## Getting Started:
**Installation:** Start with a vanilla Arch Linux installation.

**Clone Repository:** Clone the MagicArch repository to your local machine.

**Run Script:** Execute the install.sh script to initiate the setup process.

... or just run the quick install command that does not require anything besides sudo and curl

**[optional] Customize:** Modify the provided configuration files (if you wish).

<br>

## Manual Install

### requirements

- git
- git-lfs
- sudo
- ansible-core

**install required packages (run as root):**

```bash
pacman -S git git-lfs sudo ansible-core
```

**install required ansible collections (do not run as root):**

```bash
ansible-galaxy collection install community.general ansible.posix community.postgresql
```

### install

**clone the repository:**

```bash
git clone https://gitlab.com/MagicBytes/magicarch
```

**change to the newly cloned repo:**

```bash
cd magicarch
```

**install Git LFS for this repo (do not run as root):**

```bash
git lfs install
```

**pull the MagicArch KDE config from Git LFS (do not run as root):**

```bash
git-lfs pull
```

**install MagicArch (do not run as root):**

```bash
ansible-playbook site.yml --connection=local --ask-become-pass
```

<br><br>

## List of included packages

<details>
<summary>Click to expand</summary>

* wget
* unzip
* unrar
* nano
* vim
* htop
* btop
* cmake
* neofetch
* lsb-release
* flat-remix
* net-tools
* ttf-meslo-nerd-font-powerlevel10k
* update-grub
* python
* python-pip
* python-setuptools
* python-ldap
* bpython
* python-systemd
* nmap
* socat
* rlwrap
* thefuck
* dfc
* zsh
* openvpn
* jdk-openjdk
* firefox
* jdk11-openjdk
* neo4j-community
* bloodhound
* bloodhound-python
* xorg-server-xvfb
* ffuf
* feroxbuster
* ruby
* ruby-irb
* postgresql
* python-impacket-git
* enum4linux-ng-git
* kerbrute-git
* blackarch/crackmapexec
* smbmap
* wafw00f
* nikto
* commix
* davtest
* httrack
* joomscan
* whatweb
* wpscan
* sqlitebrowser
* sqlmap-git
* cewl
* crunch
* evil-winrm
* hash-identifier
* hashcat
* ananicy-cpp-git
* cachyos-ananicy-rules-git
* vscodium
* hydra
* john
* python-pypykatz
* onesixtyone
* patator
* ncrack
* aircrack-ng
* reaver
* wifite
* android-apktool
* rz-cutter
* edb
* edb-debug
* ghidra
* r2ghidra
* rz-ghidra
* jadx
* ollydbg
* radare2
* beef
* set
* bettercap
* macchanger
* responder
* tcpdump
* wireshark-qt
* zeek
* proxychains-ng
* veil
* binwalk
* foremost
* pdf-parser
* pdfid
* backdoor-factory
* bdfproxy
* soapui
* cryptsetup
* audacity
* dbeaver
* filezilla
* ghex
* keepassxc
* powershell-bin
* sonic-visualiser
* git-dumper
* jq
* openldap
* ntp
* nfs-utils
* openssh
* mariadb-clients
* nodejs
* npm
* php
* phpggc
* certipy
* dotnet-runtime
* dotnet-sdk
* dotnet-targeting-pack
* dotnet-host
* python-updog
* ngrok
* docker
* redis
* imagemagick
* rust
* strace
* clamav
* bind
* smtp-user-enum
* steghide
* stegseek
* payloadsallthethings
* windows-binaries
* xsser
* lsd
* bat
* unclutter
* profile-sync-daemon
* android-sdk-platform-tools
* samba 
* burpsuite
* intel-ucode / amd-ucode
* intel-media-driver
* intel-media-sdk
* thorium-browser-avx-bin / thorium-browser-sse3-bin
* burpsuite / burpsuite-pro
* ldapdomaindump
* cookie-monster
* wordlists
* pince-git
* metasploit-git
* pacman-contrib
* rsync
* reflector
* geoip
* informant
* alhp-keyring
* alhp-mirrorlist
* base-devel
* git
* yay
* chaotic-keyring
* chaotic-mirrorlist
* archstrike-keyring
* archstrike-mirrorlist
* python-psycopg2
* llvm
* clang
* lld
* ccache
* thermald
* nohang
* haveged
* rng-tools
* dbus-broker
* linux-cachyos
* linux-cachyos-headers
* konsave
* python-oletools

</details>

<br>

## List of included packages in /opt

<details>
<summary>Click to expand</summary>

* chisel
* chisel.exe
* Certify.exe
* Rubeus.exe
* Seatbelt.exe
* JuicyPotatoNG.exe
* krbrelayx
* LaZagne.exe
* ligolo-ng
* LinEnum.sh
* linpeas.sh
* mimikatz
* static nc
* static ncat
* static nmap
* ntlm_theft
* peirates
* phpggc
* plink.exe
* Powermad
* PowerSploit
* PowerView
* PsBypassCLM.exe
* pspy64
* PwnKit
* RunasCs.exe
* SharpHound.exe
* SharpHound.ps1
* socatx64.exe
* stegsolve
* username-anarchy
* windapsearch
* winPEAS.exe
* winPEAS.bat
* winPEAS.ps1
* XSS-cookie-stealer.py
* passthecert.py
* smtpd.py
* ssh-mitm
* CodemerxDecompile
* spose
* Bad-Pdf

</details>

<br>

## Screenshots

**neofetch screenshot**:
![Neofetch Screenshot](screenshots/neofetch.png)

**KDE workspace screenshot**:
![KDE Workspace Screenshot](screenshots/kde_screenshot.png)

**KDE menu**:
![KDE Menu Screenshot](screenshots/kde_menu_screenshot.png)

**1080p wallpaper**:
![1080p Wallpaper](wallpapers/MagicArch_Wallpaper_1080p.png)

**btop++ screenshot**:
![Btop++ Screenshot](screenshots/btop++_screenshot.png)

**colorful ls output**:
![ls Screenshot](screenshots/ls_screenshot.png)

**colorful cat output**:
![cat Screenshot](screenshots/cat_screenshot.png)

**colorful system utilities**:
![system utilities Screenshot](screenshots/colorful_system_utilities.png)

<br>

## File Structure:
**Ansible Configuration**: Configure Ansible behavior and settings in ansible.cfg.

**Inventory**: Define hosts to be managed by Ansible in inventory/hosts.ini. (default is localhost but remote hosts via ssh are also possible)

**Group Variables**: Customize settings for different host groups in group_vars/.

**Playbooks**: Execute specific tasks with Ansible playbooks in playbooks/.

**Roles**: Organize tasks and configurations into reusable roles in roles/, covering a wide range of functionalities from repository setup to desktop customization and performance optimization.

<br>

## Contributing:
Contributions to MagicArch are welcome! Feel free to fork the repository, make improvements or add new features, and submit pull requests.

<br>

## License
![GNU GPLv3 Image](https://www.gnu.org/graphics/gplv3-127x51.png)

This program is Free Software: You can use, study share and improve it at your
will. Specifically you can redistribute and/or modify it under the terms of the
[GNU General Public License](https://www.gnu.org/licenses/gpl.html) as
published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

<br>

## Credits:
MagicArch is developed and maintained by MagicBytes, with inspiration and contributions from the open-source community.

<br>

## Support:
For any questions, issues, or suggestions, please refer to the README.md or open an issue on the repository.

<br>

## Acknowledgements:
MagicArch includes logos and wallpapers provided in the logos/ and wallpapers/ directories, respectively.

<br>

## Disclaimer: 
MagicArch is provided as-is and without warranty. Use at your own risk.